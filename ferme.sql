drop database if exists ferme;
create database ferme;
use ferme;

create table bat (lettre varchar(1) primary key not null UNIQUE);

create table logement (
  id int primary key auto_increment,
  num int,
  lettre_bat varchar(1),
  nbr_place int,
  CHECK (nbr_place BETWEEN 1 AND 4),
  FOREIGN KEY (lettre_bat) REFERENCES bat(lettre),
  CONSTRAINT id_pair UNIQUE (num,lettre_bat)
);

create table espece (
  id int primary key auto_increment,
  nom varchar(255) not null,
  UNIQUE (nom)
);

create table animal (
  id int primary key auto_increment,
  nom varchar(255) not null,
  id_logement int,
  id_espece int,
  FOREIGN KEY (id_logement) REFERENCES logement(id),
  FOREIGN KEY (id_espece) references espece(id)
);

create table chantier (
  id int primary key auto_increment,
  nom varchar(255) not null,
  etat float
);

create table anim_chantier (
  id_animal int not null,
  id_chantier int not null,
  FOREIGN KEY (id_animal) REFERENCES animal(id),
  FOREIGN KEY (id_chantier) REFERENCES chantier(id),
  UNIQUE (id_animal, id_chantier)
);

#serie de test
-- insert into bat values ('W');
-- select * from bat;
-- insert into logement values (null, 1, 'W', 1);
-- select * from logement;
-- insert into logement values (null, 2, 'W', 4);
-- select * from logement where lettre_bat='W' and num=2;
-- insert into logement values (null, 3, 'W',2);
-- select * from logement;
-- SELECT SUM(nbr_place) as nbr_place_total FROM logement;
-- insert into espece values (null, 'poule');
-- insert into animal values (null, 'Huguette', 1, 1);
-- insert into animal values (null, 'le coq clodo', null, null);
-- select * from animal;
-- select nom from animal where id_logement in (select id from logement where lettre_bat='W' and num=1);
-- select animal.nom, espece.nom from animal, espece where animal.id_espece = espece.id;
-- select concat(lettre_bat, num) as logement, animal.nom as animal, espece.nom as espece from logement, animal, espece where lettre_bat='W' and num=1 and id_logement in (select id from logement where lettre_bat='W' and num=1) and animal.id_espece = espece.id;
-- SELECT SUM(nbr_place)-(SELECT COUNT(id_logement) FROM animal where id_logement is not null) as nbr_place_dispo FROM logement;
-- créer 100 logement dans le batiment A

-- DELIMITER //
-- CREATE PROCEDURE createLocation (IN bat CHAR(1), IN nb INT)
--        BEGIN
--          SET @c = 0;
--          WHILE (@c < nb) DO
--          insert into logement values (null, @c+1, bat, round(rand()*3)+1);
--          SET @c = @c+1;
--          END WHILE;
--        END//
--
-- DELIMITER ;
-- CALL createLocation('A', 100);
-- select * from logement;

DELIMITER //
CREATE PROCEDURE createBat (IN nb INT)
       BEGIN
         SET @i = 0;
         SET @c = 65;
         SET @d = 91;
         WHILE (@c < @d) DO
           insert into bat values (CHAR(@c using utf8));
           SET @i = 0;
           WHILE (@i < nb) DO
             insert into logement values (null, @i+1, CHAR(@c using utf8), round(rand()*3)+1);
             SET @i = @i+1;
           END WHILE;
           SET @c = @c+1;
         END WHILE;
       END//

DELIMITER ;
CALL createBat(100);
select * from logement;


DELIMITER //
CREATE PROCEDURE createAnimal (IN nb INT)
       BEGIN
         SET @i = 0;
         insert into espece values (null, 'dragon');
         WHILE (@i < nb) DO
            insert into animal values (null, concat('dragon', ' ', @i+1), null, 1);
            SET @i = @i+1;
         END WHILE;
       END//

DELIMITER ;
CALL createAnimal(2000);
select * from animal;
